$(window).resize(function(){
    $("html").css("height", $(window).height());
    $("body").height($(window).height());
});

$(document).ready(function(){
    // kick it off!
    load_welcome();
    $(window).trigger("resize");
});

function load_welcome(){
    console.log("loading connect into container");

    $("#container").load("connect.htm", function(){
        $("#connectBtn").click(function(){
            load_dashboard();
        });
    });
}

function load_dashboard(){
    console.log("loading dashboard into container");

    $("#container").load("dashboard.htm", function(){
        console.log("Dashboard loaded");

        load_memory_chart();
        load_fileDescriptor_table();
        load_Process_table();
    });
}

function load_memory_chart(){
    console.log("loading in highchart for memory");
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    $('#memoryData').highcharts({
        chart: {
            type: 'spline',
            animation: Highcharts.svg, // don't animate in old IE
            marginRight: 10,
            events: {
                load: function () {

                    // set up the updating of the chart each second
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime(), // current time
                            y = Math.random();
                        series.addPoint([x, y], true, true);
                    }, 1000);
                }
            },
            backgroundColor:null
        },
        title: {
            text: 'Polled Memory Output '
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: 'Memory Used'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [{
            name: 'Random data',
            data: (function () {
                // generate an array of random data
                var data = [],
                    time = (new Date()).getTime(),
                    i;

                for (i = -19; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 1000,
                        y: Math.random()
                    });
                }
                return data;
            }())
        }]
    });
}

function load_fileDescriptor_table(){

    var data = [
        {
            "name": "bootstrap-table",
            "stargazers_count": "526",
            "forks_count": "122"
        },
        {
            "name": "multiple-select",
            "stargazers_count": "288",
            "forks_count": "150"
        },
        {
            "name": "bootstrap-show-password",
            "stargazers_count": "32",
            "forks_count": "11"
        },
        {
            "name": "blog",
            "stargazers_count": "13",
            "forks_count": "4"
        },
        {
            "name": "scutech-redmine",
            "stargazers_count": "6",
            "forks_count": "3"
        }
    ];

    $('#fileDescriptorData table').bootstrapTable({
        data: data
    });
}

function load_Process_table(){

    var data = [
        {
            "name": "bootstrap-table",
            "stargazers_count": "526",
            "forks_count": "122"
        },
        {
            "name": "multiple-select",
            "stargazers_count": "288",
            "forks_count": "150"
        },
        {
            "name": "bootstrap-show-password",
            "stargazers_count": "32",
            "forks_count": "11"
        },
        {
            "name": "blog",
            "stargazers_count": "13",
            "forks_count": "4"
        },
        {
            "name": "scutech-redmine",
            "stargazers_count": "6",
            "forks_count": "3"
        }
    ];

    $('#processData table').bootstrapTable({
        data: data
    });
}

function actionFormatter(value, row, index) {
    return '<button class="btn btn-default"><i class="glyphicon glyphicon-remove"></i> Kill Process</button>';
}

window.actionEvents = {
    'click .remove': function (e, value, row, index) {
        alert('You clicked the kill switch on row: ' + JSON.stringify(row));
        console.log(value, row, index);
    }
};
